#include "chartwindow.h"
#include "ui_chartwindow.h"

ChartWindow::ChartWindow(QVector<double> data, QWidget *parent) :
    QDialog(parent), ui(new Ui::ChartWindow)
{
    ui->setupUi(this);

    _data = data;

    ui->plot->addGraph();
    ui->plot->xAxis->setRange(0, 100);
    ui->plot->yAxis->setRange(0, 100);

    qreal maxElement = 0;
    qreal minElement = 0;
    for (int i = 0; i < _data.size(); ++i){
        if (maxElement < _data[i]){
            maxElement = _data[i];
        }
    }

    for (int i = 0; i < _data.size(); ++i){
        if (minElement > _data[i]){
            minElement = _data[i];
        }
    }

    ui->plot->yAxis->setRange(minElement+10, maxElement+10);
    ui->plot->xAxis->setRange(0, _data.size());

    ui->plot->xAxis->setVisible(false);

    for (int i = 0; i < _data.size(); ++i){
        addPoint(_data[i], i);
    }

    plot();
}

ChartWindow::~ChartWindow()
{
    delete ui;
}

void ChartWindow::addPoint(double x, double y)
{
    _x.append(x);
    _y.append(y);
}

void ChartWindow::plot()
{
    ui->plot->graph(0)->setData(_y, _x);
    ui->plot->replot();
    ui->plot->update();
}
