/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QGroupBox *gbSender;
    QVBoxLayout *verticalLayout_4;
    QPlainTextEdit *ptOriginMessageText;
    QPlainTextEdit *ptOriginMessageByteText;
    QWidget *widget;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *gbCommunicationChannel;
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *ptCommunicationSignal;
    QVBoxLayout *verticalLayout_3;
    QPushButton *pbShowChart;
    QPushButton *pbTransmitMessage;
    QGroupBox *gbReceiver;
    QVBoxLayout *verticalLayout_5;
    QPlainTextEdit *ptResieverMessageText;
    QPlainTextEdit *ptResieverMessageByteText;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(420, 240);
        MainWindow->setMinimumSize(QSize(420, 240));
        MainWindow->setMaximumSize(QSize(420, 16777215));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setMinimumSize(QSize(0, 0));
        centralWidget->setMaximumSize(QSize(16777215, 16777215));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(3, 3, 3, 3);
        gbSender = new QGroupBox(centralWidget);
        gbSender->setObjectName(QStringLiteral("gbSender"));
        verticalLayout_4 = new QVBoxLayout(gbSender);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(3, 3, 3, 3);
        ptOriginMessageText = new QPlainTextEdit(gbSender);
        ptOriginMessageText->setObjectName(QStringLiteral("ptOriginMessageText"));

        verticalLayout_4->addWidget(ptOriginMessageText);

        ptOriginMessageByteText = new QPlainTextEdit(gbSender);
        ptOriginMessageByteText->setObjectName(QStringLiteral("ptOriginMessageByteText"));
        ptOriginMessageByteText->setMinimumSize(QSize(0, 120));
        ptOriginMessageByteText->setReadOnly(true);
        ptOriginMessageByteText->setOverwriteMode(false);

        verticalLayout_4->addWidget(ptOriginMessageByteText);


        horizontalLayout->addWidget(gbSender);

        widget = new QWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setMinimumSize(QSize(80, 0));
        widget->setMaximumSize(QSize(90, 16777215));
        verticalLayout_2 = new QVBoxLayout(widget);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        gbCommunicationChannel = new QGroupBox(widget);
        gbCommunicationChannel->setObjectName(QStringLiteral("gbCommunicationChannel"));
        gbCommunicationChannel->setMinimumSize(QSize(80, 0));
        gbCommunicationChannel->setMaximumSize(QSize(80, 16777215));
        verticalLayout = new QVBoxLayout(gbCommunicationChannel);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(3, 0, 3, 6);
        ptCommunicationSignal = new QPlainTextEdit(gbCommunicationChannel);
        ptCommunicationSignal->setObjectName(QStringLiteral("ptCommunicationSignal"));
        ptCommunicationSignal->setEnabled(true);
        ptCommunicationSignal->setReadOnly(true);

        verticalLayout->addWidget(ptCommunicationSignal);


        verticalLayout_2->addWidget(gbCommunicationChannel);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(3);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(3, 3, 3, 3);
        pbShowChart = new QPushButton(widget);
        pbShowChart->setObjectName(QStringLiteral("pbShowChart"));

        verticalLayout_3->addWidget(pbShowChart);

        pbTransmitMessage = new QPushButton(widget);
        pbTransmitMessage->setObjectName(QStringLiteral("pbTransmitMessage"));
        pbTransmitMessage->setMinimumSize(QSize(0, 40));
        pbTransmitMessage->setAutoFillBackground(false);

        verticalLayout_3->addWidget(pbTransmitMessage);


        verticalLayout_2->addLayout(verticalLayout_3);


        horizontalLayout->addWidget(widget);

        gbReceiver = new QGroupBox(centralWidget);
        gbReceiver->setObjectName(QStringLiteral("gbReceiver"));
        verticalLayout_5 = new QVBoxLayout(gbReceiver);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(3, 3, 3, 3);
        ptResieverMessageText = new QPlainTextEdit(gbReceiver);
        ptResieverMessageText->setObjectName(QStringLiteral("ptResieverMessageText"));
        ptResieverMessageText->setReadOnly(true);

        verticalLayout_5->addWidget(ptResieverMessageText);

        ptResieverMessageByteText = new QPlainTextEdit(gbReceiver);
        ptResieverMessageByteText->setObjectName(QStringLiteral("ptResieverMessageByteText"));
        ptResieverMessageByteText->setMinimumSize(QSize(0, 120));
        ptResieverMessageByteText->setReadOnly(true);

        verticalLayout_5->addWidget(ptResieverMessageByteText);


        horizontalLayout->addWidget(gbReceiver);

        MainWindow->setCentralWidget(centralWidget);
        gbReceiver->raise();
        widget->raise();
        gbSender->raise();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "ChaoMess", Q_NULLPTR));
        gbSender->setTitle(QApplication::translate("MainWindow", "\320\237\320\265\321\200\320\265\320\264\320\260\321\202\321\207\320\270\320\272", Q_NULLPTR));
        ptOriginMessageText->setPlainText(QString());
        ptOriginMessageByteText->setPlainText(QString());
        gbCommunicationChannel->setTitle(QApplication::translate("MainWindow", "\320\232\320\260\320\275\320\260\320\273 \321\201\320\262\321\217\320\267\320\270", Q_NULLPTR));
        ptCommunicationSignal->setPlainText(QString());
        pbShowChart->setText(QApplication::translate("MainWindow", "\320\223\321\200\320\260\321\204\320\270\320\272", Q_NULLPTR));
        pbTransmitMessage->setText(QApplication::translate("MainWindow", "\320\237\320\265\321\200\320\265\320\264\320\260\321\202\321\214\n"
"\321\201\320\276\320\276\320\261\321\211\320\265\320\275\320\270\320\265", Q_NULLPTR));
        gbReceiver->setTitle(QApplication::translate("MainWindow", "\320\237\321\200\320\270\321\221\320\274\320\275\320\270\320\272", Q_NULLPTR));
        ptResieverMessageText->setPlainText(QString());
        ptResieverMessageByteText->setPlainText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
