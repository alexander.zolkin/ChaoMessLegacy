/********************************************************************************
** Form generated from reading UI file 'chartwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHARTWINDOW_H
#define UI_CHARTWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QVBoxLayout>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_ChartWindow
{
public:
    QVBoxLayout *verticalLayout;
    QCustomPlot *plot;

    void setupUi(QDialog *ChartWindow)
    {
        if (ChartWindow->objectName().isEmpty())
            ChartWindow->setObjectName(QStringLiteral("ChartWindow"));
        ChartWindow->resize(1000, 750);
        ChartWindow->setMinimumSize(QSize(450, 300));
        ChartWindow->setWindowOpacity(1.5);
        ChartWindow->setModal(false);
        verticalLayout = new QVBoxLayout(ChartWindow);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        plot = new QCustomPlot(ChartWindow);
        plot->setObjectName(QStringLiteral("plot"));

        verticalLayout->addWidget(plot);


        retranslateUi(ChartWindow);

        QMetaObject::connectSlotsByName(ChartWindow);
    } // setupUi

    void retranslateUi(QDialog *ChartWindow)
    {
        ChartWindow->setWindowTitle(QApplication::translate("ChartWindow", "\320\223\321\200\320\260\321\204\320\270\320\272", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ChartWindow: public Ui_ChartWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHARTWINDOW_H
