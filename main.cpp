#include "mainwindow.h"
#include "rtwtypes.h"
#include <QApplication>

int_T main(int_T argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow mw;
    mw.show();

    return a.exec();
}
