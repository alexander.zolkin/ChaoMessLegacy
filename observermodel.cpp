#include "observermodel.h"
#define NumBitsPerChar                 8U

// Private macros used by the generated code to access rtModel
#ifndef rtmIsMajorTimeStep
# define rtmIsMajorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MAJOR_TIME_STEP)
#endif

#ifndef rtmIsMinorTimeStep
# define rtmIsMinorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MINOR_TIME_STEP)
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               ((rtm)->Timing.t)
#endif

#ifndef rtmSetTPtr
# define rtmSetTPtr(rtm, val)          ((rtm)->Timing.t = (val))
#endif

// Used by FromWorkspace Block: '<S5>/FromWs'
#ifndef rtInterpolate
# define rtInterpolate(v1,v2,f1,f2)    (((v1)==(v2))?((double)(v1)): (((f1)*((double)(v1)))+((f2)*((double)(v2)))))
#endif

#ifndef rtRound
# define rtRound(v)                    ( ((v) >= 0) ? std::floor((v) + 0.5) : std::ceil((v) - 0.5) )
#endif

extern real_T rt_powd_snf(real_T u0, real_T u1);
extern real_T rt_urand_Upu32_Yd_f_pw_snf(uint32_T *u);

// private model entry point functions
extern void Observer_model_derivatives();
extern "C" {
  extern real_T rtGetInf(void);
  extern real32_T rtGetInfF(void);
  extern real_T rtGetMinusInf(void);
  extern real32_T rtGetMinusInfF(void);
}                                      // extern "C"
  extern "C"
{
  extern real_T rtGetNaN(void);
  extern real32_T rtGetNaNF(void);
}                                      // extern "C"

extern "C" {
  extern real_T rtInf;
  extern real_T rtMinusInf;
  extern real_T rtNaN;
  extern real32_T rtInfF;
  extern real32_T rtMinusInfF;
  extern real32_T rtNaNF;
  extern void rt_InitInfAndNaN(size_t realSize);
  extern boolean_T rtIsInf(real_T value);
  extern boolean_T rtIsInfF(real32_T value);
  extern boolean_T rtIsNaN(real_T value);
  extern boolean_T rtIsNaNF(real32_T value);
  typedef struct {
    struct {
      uint32_T wordH;
      uint32_T wordL;
    } words;
  } BigEndianIEEEDouble;

  typedef struct {
    struct {
      uint32_T wordL;
      uint32_T wordH;
    } words;
  } LittleEndianIEEEDouble;

  typedef struct {
    union {
      real32_T wordLreal;
      uint32_T wordLuint;
    } wordL;
  } IEEESingle;
}                                      // extern "C"
  extern "C"
{
  real_T rtInf;
  real_T rtMinusInf;
  real_T rtNaN;
  real32_T rtInfF;
  real32_T rtMinusInfF;
  real32_T rtNaNF;
}

extern "C" {
  //
  // Initialize rtInf needed by the generated code.
  // Inf is initialized as non-signaling. Assumes IEEE.
  //
  real_T rtGetInf(void)
  {
    size_t bitsPerReal = sizeof(real_T) * (NumBitsPerChar);
    real_T inf = 0.0;
    if (bitsPerReal == 32U) {
      inf = rtGetInfF();
    } else {
      union {
        LittleEndianIEEEDouble bitVal;
        real_T fltVal;
      } tmpVal;

      tmpVal.bitVal.words.wordH = 0x7FF00000U;
      tmpVal.bitVal.words.wordL = 0x00000000U;
      inf = tmpVal.fltVal;
    }

    return inf;
  }

  //
  // Initialize rtInfF needed by the generated code.
  // Inf is initialized as non-signaling. Assumes IEEE.
  //
  real32_T rtGetInfF(void)
  {
    IEEESingle infF;
    infF.wordL.wordLuint = 0x7F800000U;
    return infF.wordL.wordLreal;
  }

  //
  // Initialize rtMinusInf needed by the generated code.
  // Inf is initialized as non-signaling. Assumes IEEE.
  //
  real_T rtGetMinusInf(void)
  {
    size_t bitsPerReal = sizeof(real_T) * (NumBitsPerChar);
    real_T minf = 0.0;
    if (bitsPerReal == 32U) {
      minf = rtGetMinusInfF();
    } else {
      union {
        LittleEndianIEEEDouble bitVal;
        real_T fltVal;
      } tmpVal;

      tmpVal.bitVal.words.wordH = 0xFFF00000U;
      tmpVal.bitVal.words.wordL = 0x00000000U;
      minf = tmpVal.fltVal;
    }

    return minf;
  }

  //
  // Initialize rtMinusInfF needed by the generated code.
  // Inf is initialized as non-signaling. Assumes IEEE.
  //
  real32_T rtGetMinusInfF(void)
  {
    IEEESingle minfF;
    minfF.wordL.wordLuint = 0xFF800000U;
    return minfF.wordL.wordLreal;
  }
}
  extern "C"
{
  //
  // Initialize rtNaN needed by the generated code.
  // NaN is initialized as non-signaling. Assumes IEEE.
  //
  real_T rtGetNaN(void)
  {
    size_t bitsPerReal = sizeof(real_T) * (NumBitsPerChar);
    real_T nan = 0.0;
    if (bitsPerReal == 32U) {
      nan = rtGetNaNF();
    } else {
      union {
        LittleEndianIEEEDouble bitVal;
        real_T fltVal;
      } tmpVal;

      tmpVal.bitVal.words.wordH = 0xFFF80000U;
      tmpVal.bitVal.words.wordL = 0x00000000U;
      nan = tmpVal.fltVal;
    }

    return nan;
  }

  //
  // Initialize rtNaNF needed by the generated code.
  // NaN is initialized as non-signaling. Assumes IEEE.
  //
  real32_T rtGetNaNF(void)
  {
    IEEESingle nanF = { { 0 } };

    nanF.wordL.wordLuint = 0xFFC00000U;
    return nanF.wordL.wordLreal;
  }
}

extern "C" {
  //
  // Initialize the rtInf, rtMinusInf, and rtNaN needed by the
  // generated code. NaN is initialized as non-signaling. Assumes IEEE.
  //
  void rt_InitInfAndNaN(size_t realSize)
  {
    (void) (realSize);
    rtNaN = rtGetNaN();
    rtNaNF = rtGetNaNF();
    rtInf = rtGetInf();
    rtInfF = rtGetInfF();
    rtMinusInf = rtGetMinusInf();
    rtMinusInfF = rtGetMinusInfF();
  }

  // Test if value is infinite
  boolean_T rtIsInf(real_T value)
  {
    return (boolean_T)((value==rtInf || value==rtMinusInf) ? 1U : 0U);
  }

  // Test if single-precision value is infinite
  boolean_T rtIsInfF(real32_T value)
  {
    return (boolean_T)(((value)==rtInfF || (value)==rtMinusInfF) ? 1U : 0U);
  }

  // Test if value is not a number
  boolean_T rtIsNaN(real_T value)
  {
    return (boolean_T)((value!=value) ? 1U : 0U);
  }

  // Test if single-precision value is not a number
  boolean_T rtIsNaNF(real32_T value)
  {
    return (boolean_T)(((value!=value) ? 1U : 0U));
  }
}
//
// This function updates continuous states using the ODE3 fixed-step
// solver algorithm
//
  void ObserverModelClass::rt_ertODEUpdateContinuousStates(RTWSolverInfo *si,
   QVector<real_T>& transmitterData, QVector<real_T>& receiverData,
   QVector<time_T>& sendedSignal, QVector<time_T>& time, qreal signal)
{
  // Solver Matrices
  static const real_T rt_ODE3_A[3] = {
    1.0/2.0, 3.0/4.0, 1.0
  };

  static const real_T rt_ODE3_B[3][3] = {
    { 1.0/2.0, 0.0, 0.0 },

    { 0.0, 3.0/4.0, 0.0 },

    { 2.0/9.0, 1.0/3.0, 4.0/9.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE3_IntgData *id = (ODE3_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T hB[3];
  int_T i;
  int_T nXc = 6;
  rtsiSetSimTimeStep(si, MINOR_TIME_STEP);

  // Save the state values at time t in y, we'll use x as ynew.
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  // Assumes that rtsiSetT and ModelOutputs are up-to-date
  // f0 = f(t,y)
  rtsiSetdX(si, f0);
  Observer_model_derivatives();

  // f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*));
  hB[0] = h * rt_ODE3_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[0]);
  rtsiSetdX(si, f1);
  this->step0(transmitterData, receiverData, sendedSignal, time, signal);
  Observer_model_derivatives();

  // f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*));
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE3_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[1]);
  rtsiSetdX(si, f2);
  this->step0(transmitterData, receiverData, sendedSignal, time, signal);
  Observer_model_derivatives();

  // tnew = t + hA(3);
  // ynew = y + f*hB(:,3);
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE3_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, tnew);
  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

real_T rt_powd_snf(real_T u0, real_T u1)
{
  real_T y;
  real_T tmp;
  real_T tmp_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else {
    tmp = std::abs(u0);
    tmp_0 = std::abs(u1);
    if (rtIsInf(u1)) {
      if (tmp == 1.0) {
        y = 1.0;
      } else if (tmp > 1.0) {
        if (u1 > 0.0) {
          y = (rtInf);
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = (rtInf);
      }
    } else if (tmp_0 == 0.0) {
      y = 1.0;
    } else if (tmp_0 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = std::sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > std::floor(u1))) {
      y = (rtNaN);
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

real_T rt_urand_Upu32_Yd_f_pw_snf(uint32_T *u)
{
  uint32_T lo;
  uint32_T hi;

  // Uniform random number generator (random number between 0 and 1)

  // #define IA      16807                      magic multiplier = 7^5
  // #define IM      2147483647                 modulus = 2^31-1
  // #define IQ      127773                     IM div IA
  // #define IR      2836                       IM modulo IA
  // #define S       4.656612875245797e-10      reciprocal of 2^31-1
  // test = IA * (seed % IQ) - IR * (seed/IQ)
  // seed = test < 0 ? (test + IM) : test
  // return (seed*S)

  lo = *u % 127773U * 16807U;
  hi = *u / 127773U * 2836U;
  if (lo < hi) {
    *u = 2147483647U - (hi - lo);
  } else {
    *u = lo - hi;
  }

  return (real_T)*u * 4.6566128752457969E-10;
}

// Model step function for TID0
void ObserverModelClass::step0(QVector<real_T>& transmitterData, QVector<real_T>& receiverData,
                               QVector<time_T>& sendedSignal, QVector<time_T>& time, qreal signal) // Sample time: [0.0s, 0.0s]
{
  if (rtmIsMajorTimeStep((&rtM))) {
    // set solver stop time
    rtsiSetSolverStopTime(&(&rtM)->solverInfo,(((&rtM)->Timing.clockTick0+1)*
      (&rtM)->Timing.stepSize0));
  }                                    // end MajorTimeStep

  // Update absolute time of base rate at minor time step
  if (rtmIsMinorTimeStep((&rtM))) {
    (&rtM)->Timing.t[0] = rtsiGetT(&(&rtM)->solverInfo);
  }

  // FromWorkspace: '<S5>/FromWs'
  {
    real_T *pDataValues = (real_T *) rtDW.FromWs_PWORK.DataPtr;
    real_T *pTimeValues = (real_T *) rtDW.FromWs_PWORK.TimePtr;
    int_T currTimeIndex = rtDW.FromWs_IWORK.PrevIndex;
    real_T t = (&rtM)->Timing.t[0];

    // Get index
    if (t <= pTimeValues[0]) {
      currTimeIndex = 0;
    } else if (t >= pTimeValues[31]) {
      currTimeIndex = 30;
    } else {
      if (t < pTimeValues[currTimeIndex]) {
        while (t < pTimeValues[currTimeIndex]) {
          currTimeIndex--;
        }
      } else {
        while (t >= pTimeValues[currTimeIndex + 1]) {
          currTimeIndex++;
        }
      }
    }

    rtDW.FromWs_IWORK.PrevIndex = currTimeIndex;

    // Post output
    {
      real_T t1 = pTimeValues[currTimeIndex];
      real_T t2 = pTimeValues[currTimeIndex + 1];
      if (t1 == t2) {
        if (t < t1) {
          rtDW.FromWs = pDataValues[currTimeIndex];
        } else {
          rtDW.FromWs = pDataValues[currTimeIndex + 1];
        }
      } else {
        real_T f1 = (t2 - t) / (t2 - t1);
        real_T f2 = 1.0 - f1;
        real_T d1;
        real_T d2;
        int_T TimeIndex= currTimeIndex;
        d1 = pDataValues[TimeIndex];
        d2 = pDataValues[TimeIndex + 1];
        rtDW.FromWs = (real_T) rtInterpolate(d1, d2, f1, f2);
        pDataValues += 32;
      }
    }
  }

  // Integrator: '<S2>/Integrator 5'
  rtDW.Integrator5 = rtX.Integrator5_CSTATE;

  // Integrator: '<S3>/Integrator 1'
  rtDW.Integrator1 = rtX.Integrator1_CSTATE;

  // Fcn: '<S2>/estimation' incorporates:
  //   Constant: '<Root>/alpha'
  //   Integrator: '<S2>/Integrator 6'

  rtDW.estimation = (1.0 * rtDW.Integrator5 * rtDW.Integrator1 + 1.0) +
    rtX.Integrator6_CSTATE;

  // Sum: '<S2>/Sum' incorporates:
  //   Constant: '<Root>/b'

  rtDW.Sum = rtDW.estimation - 10.0;

  // Sum: '<Root>/Sum'
  rtDW.Sum_m = rtDW.FromWs - rtDW.Sum;
  if (rtmIsMajorTimeStep((&rtM))) {
  }

  // Integrator: '<S2>/Integrator 4'
  rtDW.Integrator4 = rtX.Integrator4_CSTATE;
  if (rtmIsMajorTimeStep((&rtM))) {
  }

  // Fcn: '<S2>/dv//dt' incorporates:
  //   Constant: '<Root>/alpha'
  //   Integrator: '<S2>/Integrator 6'

  rtDW.dvdt = ((((((((-2.0 * rtDW.Integrator4 * rtDW.Integrator1 + 2.0 *
                      rtDW.Integrator1 * rtDW.Integrator5) - 2.0 * rt_powd_snf
                     (rtDW.Integrator4, 2.0)) + rt_powd_snf(rtDW.Integrator5,
    4.0)) + 2.0 * rtDW.Integrator4 * rtDW.Integrator5) - rt_powd_snf
                  (rtDW.Integrator5, 2.0)) + rtDW.Integrator1 * rtDW.Integrator5)
                - rt_powd_snf(rtDW.Integrator5, 3.0) * 1.0 * rtDW.Integrator1) -
               rt_powd_snf(rtDW.Integrator5, 2.0) * rtX.Integrator6_CSTATE) *
    1.0;

  // Integrator: '<S3>/Integrator 2'
  rtDW.Integrator2 = rtX.Integrator2_CSTATE;
  if (rtmIsMajorTimeStep((&rtM))) {
  }

  // Integrator: '<S3>/Integrator 3'
  rtDW.Integrator3 = rtX.Integrator3_CSTATE;
  if (rtmIsMajorTimeStep((&rtM))) {
  }

  // Sum: '<S3>/Sum' incorporates:
  //   Constant: '<Root>/b'

  rtDW.Sum_e = signal + 10.0;
  if (rtmIsMajorTimeStep((&rtM))) {
  }

  // Fcn: '<S3>/dz//dt'
  rtDW.dzdt = (((((2.0 * rt_powd_snf(rtDW.Integrator2, 2.0) / rtDW.Integrator3 +
                   -2.0 * rtDW.Integrator1) + rtDW.Integrator2 *
                  rtDW.Integrator1 / rtDW.Integrator3) - rt_powd_snf
                 (rtDW.Integrator3, 3.0)) - 2.0 * rtDW.Integrator2) + rtDW.Sum_e
               * rtDW.Integrator3) - rtDW.Integrator1;
  if (rtmIsMajorTimeStep((&rtM))) {
    rt_ertODEUpdateContinuousStates(&(&rtM)->solverInfo, transmitterData, receiverData,
        sendedSignal, time, signal);

    // Update absolute time
    // The "clockTick0" counts the number of times the code of this task has
    //  been executed. The absolute time is the multiplication of "clockTick0"
    //  and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
    //  overflow during the application lifespan selected.

    ++(&rtM)->Timing.clockTick0;
    (&rtM)->Timing.t[0] = rtsiGetSolverStopTime(&(&rtM)->solverInfo);

    // Update absolute time
    // The "clockTick1" counts the number of times the code of this task has
    //  been executed. The resolution of this integer timer is 0.005, which is the step size
    //  of the task. Size of "clockTick1" ensures timer will not overflow during the
    //  application lifespan selected.

//    ++i;
//    if (i > 5000){
//        qDebug() << '\n';
//    }

    receiverData.append(rtDW.Sum);
    transmitterData.append(rtDW.Integrator1);
    sendedSignal.append(rtDW.Sum_e);
//    time.append(*(&rtM)->Timing.t);

    (&rtM)->Timing.clockTick1++;
  }                                    // end MajorTimeStep
}

// Derivatives for root system: '<Root>'
void ObserverModelClass::Observer_model_derivatives()
{
  XDot *_rtXdot;
  _rtXdot = ((XDot *) (&rtM)->derivs);

  // Derivatives for Integrator: '<S2>/Integrator 6'
  _rtXdot->Integrator6_CSTATE = rtDW.dvdt;

  // Derivatives for Integrator: '<S2>/Integrator 5'
  _rtXdot->Integrator5_CSTATE = rtDW.Integrator4;

  // Derivatives for Integrator: '<S3>/Integrator 1'
  _rtXdot->Integrator1_CSTATE = rtDW.dzdt;

  // Derivatives for Integrator: '<S2>/Integrator 4'
  _rtXdot->Integrator4_CSTATE = rtDW.Integrator1;

  // Derivatives for Integrator: '<S3>/Integrator 2'
  _rtXdot->Integrator2_CSTATE = rtDW.Integrator1;

  // Derivatives for Integrator: '<S3>/Integrator 3'
  _rtXdot->Integrator3_CSTATE = rtDW.Integrator2;
}

// Model step function for TID2
void ObserverModelClass::step2() // Sample time: [5.0s, 0.0s]
{
  // Update for UniformRandomNumber: '<S1>/Uniform Random Number'
  rt_urand_Upu32_Yd_f_pw_snf(&rtDW.RandSeed);
}

// Model initialize function
void ObserverModelClass::initialize(double timeStep)
{
  // Registration code

  // initialize non-finites
  rt_InitInfAndNaN(sizeof(real_T));

  {
    // Setup solver object
    rtsiSetSimTimeStepPtr(&(&rtM)->solverInfo, &(&rtM)->Timing.simTimeStep);
    rtsiSetTPtr(&(&rtM)->solverInfo, &rtmGetTPtr((&rtM)));
    rtsiSetStepSizePtr(&(&rtM)->solverInfo, &(&rtM)->Timing.stepSize0);
    rtsiSetdXPtr(&(&rtM)->solverInfo, &(&rtM)->derivs);
    rtsiSetContStatesPtr(&(&rtM)->solverInfo, (real_T **) &(&rtM)->contStates);
    rtsiSetNumContStatesPtr(&(&rtM)->solverInfo, &(&rtM)->Sizes.numContStates);
    rtsiSetNumPeriodicContStatesPtr(&(&rtM)->solverInfo, &(&rtM)
      ->Sizes.numPeriodicContStates);
    rtsiSetPeriodicContStateIndicesPtr(&(&rtM)->solverInfo, &(&rtM)
      ->periodicContStateIndices);
    rtsiSetPeriodicContStateRangesPtr(&(&rtM)->solverInfo, &(&rtM)
      ->periodicContStateRanges);
    rtsiSetErrorStatusPtr(&(&rtM)->solverInfo, (&rtmGetErrorStatus((&rtM))));
    rtsiSetRTModelPtr(&(&rtM)->solverInfo, (&rtM));
  }

  rtsiSetSimTimeStep(&(&rtM)->solverInfo, MAJOR_TIME_STEP);
  (&rtM)->intgData.y = (&rtM)->odeY;
  (&rtM)->intgData.f[0] = (&rtM)->odeF[0];
  (&rtM)->intgData.f[1] = (&rtM)->odeF[1];
  (&rtM)->intgData.f[2] = (&rtM)->odeF[2];
  getRTM()->contStates = ((X *) &rtX);
  rtsiSetSolverData(&(&rtM)->solverInfo, (void *)&(&rtM)->intgData);
  rtsiSetSolverName(&(&rtM)->solverInfo,"ode3");
  rtmSetTPtr(getRTM(), &(&rtM)->Timing.tArray[0]);
  (&rtM)->Timing.stepSize0 = timeStep;

  {
    uint32_T tseed;
    int32_T r;
    int32_T t;

    // Start for FromWorkspace: '<S5>/FromWs'
    {
      static real_T pTimeValues0[] = { 0.0, 5.0, 5.0, 10.0, 10.0, 15.0, 15.0,
        20.0, 20.0, 25.0, 25.0, 30.0, 30.0, 35.0, 35.0, 40.0, 40.0, 45.0, 45.0,
        50.0, 50.0, 55.0, 55.0, 60.0, 60.0, 65.0, 65.0, 70.0, 70.0, 75.0, 75.0,
        80.0 } ;

      real_T pDataValues0[] = { 10.0, 10.0, 8.0, 8.0, 10.0, 10.0, 8.0,
        8.0, 10.0, 10.0, 8.0, 8.0, 10.0, 10.0, 8.0, 8.0, 10.0, 10.0, 8.0, 8.0,
        10.0, 10.0, 8.0, 8.0, 10.0, 10.0, 8.0, 8.0, 10.0, 10.0, 8.0, 8.0 } ;

      rtDW.FromWs_PWORK.TimePtr = (void *) pTimeValues0;
      rtDW.FromWs_PWORK.DataPtr = (void *) pDataValues0;
      rtDW.FromWs_IWORK.PrevIndex = 0;
    }

    // InitializeConditions for Integrator: '<S2>/Integrator 6'
    rtX.Integrator6_CSTATE = 8.0;

    // InitializeConditions for Integrator: '<S2>/Integrator 5'
    rtX.Integrator5_CSTATE = 1.0;

    // InitializeConditions for Integrator: '<S3>/Integrator 1'
    rtX.Integrator1_CSTATE = 8.0;

    // InitializeConditions for UniformRandomNumber: '<S1>/Uniform Random Number'
    tseed = (uint32_T)1.0;
    r = (int32_T)(tseed >> 16U);
    t = (int32_T)(tseed & 32768U);
    tseed = ((((tseed - ((uint32_T)r << 16U)) + t) << 16U) + t) + r;
    if (tseed < 1U) {
      tseed = 1144108930U;
    } else {
      if (tseed > 2147483646U) {
        tseed = 2147483646U;
      }
    }

    rtDW.RandSeed = tseed;
    rt_urand_Upu32_Yd_f_pw_snf(&rtDW.RandSeed);

    // End of InitializeConditions for UniformRandomNumber: '<S1>/Uniform Random Number'

    // InitializeConditions for Integrator: '<S2>/Integrator 4'
    rtX.Integrator4_CSTATE = 1.0;

    // InitializeConditions for Integrator: '<S3>/Integrator 2'
    rtX.Integrator2_CSTATE = 1.0;

    // InitializeConditions for Integrator: '<S3>/Integrator 3'
    rtX.Integrator3_CSTATE = 1.0;
  }
}

// Constructor
ObserverModelClass::ObserverModelClass(){ }

// Destructor
ObserverModelClass::~ObserverModelClass()
{
  // Currently there is no destructor body generated.
}

// Real-Time Model get method
RT_MODEL * ObserverModelClass::getRTM()
{
  return (&rtM);
}

