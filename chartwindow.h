#ifndef CHARTWINDOW_H
#define CHARTWINDOW_H

#include <QDialog>

namespace Ui {
class ChartWindow;
}

class ChartWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ChartWindow(QVector<double> data, QWidget *parent = 0);
    ~ChartWindow();

    void addPoint(double x, double y);
    void plot();

private slots:

private:
    QVector<double> _x, _y;
    QVector<double> _data;

    Ui::ChartWindow *ui;
};

#endif // CHARTWINDOW_H
