#include <stddef.h>
#include <stdio.h>
#include "observermodel.h"
#include "rtwtypes.h"
#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "signalhandler.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbTransmitMessage_clicked()
{
    QTime start = QTime::currentTime();

    if (ui->ptOriginMessageText->toPlainText().size() == 0){
        return;
    }

    QVector<real_T> receiverData;
    QVector<time_T> sendedSignal;
    QVector<time_T> time;

    int messageSize = 20000;

    QVector<QByteArray> recievedByteMessage;

    SignalHandler SC;
    QVector<QByteArray> byteMessage = SC.StrToBit(ui->ptOriginMessageText->toPlainText());


    SC.SendSignal(byteMessage, messageSize, _transmitterData, receiverData, sendedSignal, time);


    QVector<QByteArray> rawReceiverDataText = SC.ReceiveSignal(messageSize, receiverData, recievedByteMessage);
    QString recoveredMessage = SC.BitToStr(rawReceiverDataText);

    QString stringByteReceivedText;
    for (int i = 0; i < byteMessage.size(); ++i){
        stringByteReceivedText.append(byteMessage[i] + "\n");
    }

    for (int i = 0; i < receiverData.size(); i += 100){
        _stringTransmitterDataText.append(QString::number(_transmitterData[i]) + "\n");
    }

    QString stringReceiverDataText;
    for (int i = 0; i < rawReceiverDataText.size(); ++i){
        stringReceiverDataText.append(rawReceiverDataText[i]);
    }

    ui->ptOriginMessageByteText->setPlainText(stringByteReceivedText);
    ui->ptCommunicationSignal->setPlainText(_stringTransmitterDataText);
    ui->ptResieverMessageText->setPlainText(recoveredMessage);
    ui->ptResieverMessageByteText->setPlainText(stringByteReceivedText);

    qDebug() << "Отправка сигнала:" << start.elapsed() << "милисекунд.";
}

void MainWindow::on_pbShowChart_clicked()
{
    ChartWindow CW(_transmitterData);
    CW.setWindowFlags(CW.windowFlags() | Qt::WindowMinMaxButtonsHint);
    CW.exec();
}
